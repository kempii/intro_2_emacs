# intro_2_emacs

Repositorio con información introductoria sobre GNU Emacs

El archivo **intro_2_emacs.org** contiene una documentación básica basada en el tutorial inicial de Emacs.

El archivo **init.el** contiene una configuración básica de Emacs enfocada en apoyar al usuario a realizar comandos de manera más sencilla.

El archivo **my_init.el** contiene mi configuración de Emacs, considerar que los accesos a diferentes archivos deben modificarse a sus respectivos directorios en sus computadoras

La licencia de este material es **Creative Commons Attribution-NonCommercial 4.0 International**

Este repositorio fue creado por *kempii* para el taller **Introducción a (GNU) Emacs** presentado en la jornada de Software Libre organizada por *Autodefensa Digital del Norte Hackerspace* el 17 de septiembre de 2022 en el marco del [Software Freedom Day 2022](https://wiki.softwarefreedomday.org/2022).
