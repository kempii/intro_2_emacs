
* Introducci�n a (GNU) Emacs 

Por: kempii
Para: Software Freedom Day 2022
Desde: Autodefensa Digital del Norte Hackerspace


** �Qu� es Emacs?

Emacs es un *editor de texto* (de software) *libre*, que es extendible, personalizable y mucho m�s. Adem�s, en un sentido m�s avanzado, es un *interpretador de Emacs Lisp* (dialecto del lenguaje de programaci�n Lisp) con extensiones que facilitan la edici�n de texto.

Es uno de los software libres m�s antiguos que siguen en desarrollo. Originalmente escrito en 1976 por David A. Moon y Guy L. Steele Jr., mientras que la variante m�s popular, GNU Emacs, fue originalmente escrita por Richard Stallman.

(GNU) Emacs tiene licencia de GPL 3.0 o posterior (General Public License), por lo que se pueden ejercer las cuatro (4) libertades del software libre:

0. la libertad de *usar* el programa, con cualquier prop�sito (uso).
1. la libertad de *estudiar* c�mo funciona el programa y modificarlo, adapt�ndolo a las propias necesidades (estudio).
2. la libertad de *distribuir* copias del programa, con lo cual se puede ayudar a otros usuarios (distribuci�n).
3. la libertad de *mejorar* el programa y hacer p�blicas esas mejoras a los dem�s, de modo que toda la comunidad se beneficie (mejora).

/Nota: Las libertades 1 y 3 requieren acceso al c�digo fuente, porque estudiar y modificar software sin su c�digo fuente es muy poco viable./

*** �Qu� ofrece Emacs?

- Gran experiencia en edici�n de texto
- Poderosos atajos de teclado (keybindings)
- Completamente personalizable
- Funciones para casi todo, y para lo que no, t� las puedes crear
- Mucha y muy accesible documentaci�n
- Mucha comunidad activa
- Infinidad de paquetes para cualquier cosa

  
** Ya entr� a Emacs, �ahora c�mo me salgo?

Existen dos teclas muy importantes para trabajar en Emacs:

- *CONTROL*: Tambi�n llamada CTRL o CTL
- *META*: Tambi�n com�nmente llamada META

Estas dos teclas se utilizan para realizar los comandos y atajos de teclado.

En lugar de escribir completamente esto en cada ocasi�n, usaremos las siguientes abreviaturas:

  C-<car> significa mantener presionada la tecla CONTROL mientras
          teclea el car�cter <car>.  Por lo tanto C-f ser�: Mantenga
          presionada la tecla CONTROL y teclee f.
  M-<car> significa mantener presionada la tecla META o ALT mientras
          teclea <car>.  Si no hay teclas META o ALT, en su lugar
          presione y libere la tecla ESC y luego teclee <car>.
          Escribimos <ESC> para referirnos a la tecla ESC.

Ahora s�, �c�mo me salgo de Emacs? El comando para salir es:

- *C-x C-c* -> Este atajo de teclado corre el comando *save-buffers-kill-terminal*, que tambi�n pueden ejecutar de la siguiente manera *M-x save-buffers-kill-terminal*

-> /Nota importante: Para cancelar un comando parcialmente introducido, teclee C-g./


** Navegaci�n

*** Para ver pantallas completas

Los siguientes comandos son �tiles para ver pantallas completas:

	C-v 	Avanzar una pantalla completa
	M-v 	Retroceder una pantalla completa
	C-l 	Limpiar la pantalla y mostrar todo el texto de nuevo,
		 moviendo el texto alrededor del cursor al centro de la
		 pantalla (Esto es CONTROL-L, no CONTROL-1.)

*** Movimiento b�sico del cursor

/Nota: La ubicaci�n del cursor en el texto se llama tambi�n �punto�.  En otras palabras, el cursor muestra sobre la pantalla donde est� situado el punto dentro del texto./


		        L�nea anterior, C-p
			       ::
			       ::
   Atr�s, C-b.... Posici�n actual del cursor .... Adelante, C-f
			       ::
			       ::
			L�nea siguiente, C-n

                        
Resulta m�s f�cil recordar estas letras por las palabras que representan en ingl�s:

- P de Previous (anterior)
- N de Next (siguiente)
- B de Backward (atr�s)
- F de Forward (adelante).

/Vamos a estar usando estos comandos de posicionamiento b�sico del cursor todo el tiempo./

/Nota importante: Tambi�n puede mover el cursor con las teclas de flecha si su terminal dispone de ellas.  Recomendamos aprender C-b, C-f, C-n y C-p por tres razones.  Primero, funcionan en todo tipo de terminales.  Segundo, una vez que gane pr�ctica usando Emacs, encontrar� que teclear estos caracteres Control es m�s r�pido que usar teclas de flecha (porque no tendr� que mover las manos de la posici�n para mecanografiar). Tercero, una vez tenga el h�bito de usar estos comandos Control, tambi�n puede aprender a usar otros comandos avanzados de movimiento del cursor f�cilmente./

*** Extendiendo el movimiento

Si moverse por caracteres es muy lento, puedes moverte por palabras.
M-f (META-f) mueve adelante una palabra y M-b mueve atr�s una palabra.

Este paralelo se aplica entre l�neas y oraciones: C-a y C-e para moverse al comienzo o al final de la l�nea; y M-a y M-e para mover al comienzo o al final de una oraci�n.

Checa c�mo la repetici�n de C-a no hace nada, pero la repetici�n de M-a sigue moviendo una oraci�n m�s. Aunque no son muy an�logas, cada una parece natural.

Otros dos comandos importantes de movimiento del cursor son M-< (META Menor que), el cual se mueve al comienzo del texto entero, y M-> (META Mayor que), el cual se mueve al final del texto entero.

*** Argumentos num�ricos

La mayor�a de comandos de Emacs aceptan un argumento num�rico; para la mayor�a de comandos esto sirve como un factor de repetici�n.  La manera de pasarle un factor de repetici�n a un comando es tecleando C-u y luego los d�gitos antes de introducir los comandos.  Si tiene una tecla META (o ALT), hay una manera alternativa para ingresar un argumento num�rico: teclear los d�gitos mientras presiona la tecla META.  Recomendamos aprender el m�todo C-u porque �ste funciona en cualquier terminal.  El argumento num�rico es tambi�n llamado un �argumento prefijo�, porque usted teclea el argumento antes del comando al que se aplica.

Por ejemplo, C-u 8 C-f mueve hacia adelante ocho caracteres.

La mayor�a de comandos usan el argumento num�rico como un factor de repetici�n, pero algunos comandos le dan otros usos.  Varios comandos (pero ninguno de los que ha aprendido hasta ahora) lo usan como una bandera: la presencia de un argumento prefijo, sin tener en cuenta su valor, hace que el comando act�e de forma diferente.

*** Buscar texto por coincidencias

Emacs puede hacer b�squedas de cadenas (una �cadena� es un grupo de caracteres contiguos) hacia adelante a trav�s del texto o hacia atr�s en el mismo.  La b�squeda de una cadena es un comando de movimiento de cursor; mueve el cursor al pr�ximo lugar donde esa cadena aparece.

El comando de b�squeda de Emacs es �incremental�.  Esto significa que la b�squeda ocurre mientras teclea la cadena para buscarla.

El comando para iniciar una b�squeda es C-s para b�squeda hacia adelante, y C-r para la b�squeda hacia atr�s.  �PERO ESPERE!  No los intente a�n.

Cuando teclee C-s ver� que la cadena �I-search� aparece como una petici�n en el �rea de eco.  Esto le indica que Emacs est� en lo que se conoce como b�squeda incremental, esperando que teclee lo que quiere buscar.  <Return> termina una b�squeda.

Si usted teclea lo que quiere buscar y luego presiona de nuevo C-s, saltar� a la siguiente coincidencia.

*** Resumen de la navegaci�n

	C-f 	Avanzar un car�cter
	C-b 	Retroceder un car�cter

	M-f 	Avanzar una palabra
	M-b 	Retroceder una palabra

	C-n 	Avanzar a la l�nea siguiente
	C-p 	Retroceder a la l�nea anterior

	C-a 	Retroceder al comienzo de la l�nea
	C-e 	Avanzar al final de la l�nea

	M-a 	Retroceder al comienzo de la oraci�n
	M-e 	Avanzar al final de la oraci�n

 	C-v 	Avanzar una pantalla completa
	M-v 	Retroceder una pantalla completa

	M-< 	Moverse al inicio del texto entero
	M-> 	Moverser al final del texto entero

	C-u <digito> 	Argumento num�rico
        
	C-s 	B�squeda incremental
	C-r 	B�squeda hacia atr�s (tambi�n incremental)


** Ventanas, ayuda, archivos y buffers

Hay toda una serie de comandos que comienzan con CONTROL-x; muchos de ellos tienen que ver con ventanas, archivos, buffers y cosas relacionadas.  Estos comandos son de una longitud de dos, tres o cuatro caracteres.

*** Ventanas

Emacs puede tener varias �ventanas�, cada una mostrando su propio texto. Aqu� van algunos comandos para realizar gesti�n de ventanas:

	C-x 1 	Una ventana (elimina todas ventanas, excepto en la que est� el cursor)
	C-x 2 	Abre una ventana en la parte de abajo
	C-x 3 	Abre una ventana en la parte de la derecha
	C-x o 	Cambia el cursor de ventana

*** Ayuda

El comando de ayuda en Emacs es C-h

Aqu� hay algunas opciones �tiles de C-h:

   C-h x	Describe un comando.  Usted teclea el nombre del comando.
   C-h k	Describe un atajo de teclado
   C-h v	Describe una variable
   C-h f	Describe una funci�n
   C-h a 	Comando Apropos.  Teclee una palabra y Emacs har� una
		lista de todos los comandos que contengan esa palabra.
		Todos estos comandos pueden ser invocados con META-x.
		Para algunos comandos, el Comando Apropos tambi�n
		listar� una secuencia de uno o dos caracteres la cual
		ejecutar� el mismo comando.

*** Archivos

Una cosa especial acerca del comando para encontrar un archivo es que tendr� que decir qu� nombre de archivo desea.  Decimos que el comando �lee un argumento� (en este caso, el argumento es el nombre del archivo).  Despu�s de teclear el comando:

	C-x C-f   Encontrar un archivo

Emacs le pide que teclee el nombre del archivo.  El nombre de archivo que teclee aparece en la l�nea final de la pantalla.  A la l�nea final de la pantalla se la denomina minibuffer cuando se utiliza para este tipo de entradas.  Puede usar comandos de edici�n ordinarios de Emacs para editar el nombre del archivo.

Mientras est� ingresando el nombre del archivo (o cualquier otra entrada al minibuffer) puede cancelar el comando con C-g.

Cuando haya finalizado de ingresar el nombre del archivo, teclee <Return> para terminarlo.  El minibuffer desaparece, y el comando C-x C-f trabaja para encontrar el archivo que escogi�.

En seguida aparecer� el contenido del archivo en la pantalla, y puede editarlo.  Cuando quiera que sus cambios sean permanentes, teclee el comando

	C-x C-s   Guardar el archivo
	C-x C-w   "Guardar como" el archivo

Esto copia el texto dentro de Emacs al archivo.  La primera vez que haga esto, Emacs renombrar� el archivo original con un nuevo nombre para que �ste no se pierda.  El nuevo nombre se hace agregando �~� al final del nombre del archivo original. Cuando guardar haya terminado, Emacs mostrar� el nombre del archivo escrito.

Puede encontrar un archivo existente, para verlo o editarlo.  Tambi�n puede hacerlo con un archivo que no exista.  �sta es la forma de crear un archivo en Emacs.

*** Buffers

Emacs almacena cada archivo que abra dentro de un objeto llamado �buffer�.  Al encontrar un archivo se crea un nuevo buffer dentro de Emacs.  Para mirar la lista de los buffers que existen actualmente, teclee:

	C-x C-b   Lista de buffers

Cuando tenga varios buffers, solo uno de ellos es �actual� en alg�n momento.  Ese buffer es el que actualmente edita.  Si quiere editar otro buffer, necesita �cambiar� a �l.  Si quiere cambiar a un buffer que corresponde a un archivo, puede hacerlo visitando el archivo de nuevo con C-x C-f.  Pero existe una manera m�s r�pida: use el comando C-x b.  En ese comando, necesita teclear el nombre de buffer.

	C-x b 	Para cambiar a otro buffer, insertando como argumento el nombre del buffer
	C-x s 	Guarda todos los buffers sin tener que tener el cursor en ellos


** Edici�n de texto

*** Insertar

Si quieres insertar un texto, basta con que lo teclees.  Los caracteres normales, como A, 7, *, etc. se insertan nada m�s al teclearlos, al igual que los saltos de l�nea.

*** Borrar y eliminar

Para borrar el car�cter que precede al cursor, oprime <DEL>. Es una tecla alargada, normalmente etiquetada como �Backspace� o �Del�, o con una flecha apuntando a la izquierda; la misma que sueles utilizar fuera de Emacs para borrar el �ltimo car�cter introducido.

Puede haber otra tecla llamada �Del� o �Supr� en otra parte, pero �sa no es <DEL>.

Otras maneras de borrar:

	<DEL>   	Borra el car�cter justo antes del cursor
	C-d		Borra el siguiente car�cter despu�s del cursor

	M-<DEL> 	Elimina la palabra inmediatamente antes del cursor
	M-d        	Elimina la siguiente palabra despu�s del cursor

	C-k  		Elimina desde el cursor hasta el fin de la l�nea
	M-k  		Elimina hasta el final de la oraci�n actual

/Nota: Nota que <DEL> y C-d, comparados con M-<DEL> y M-d, extienden el paralelismo iniciado por C-f y M-f/

Tambi�n puede eliminar un segmento contiguo de texto con un m�todo uniforme.  Mu�vase a un extremo de ese segmento de texto, y teclee C-@ o C-<SPC> (cualquiera de los dos).  (<SPC> es la barra espaciadora.) Luego, mueva el cursor al otro extremo del texto que desea eliminar. Al hacerlo, Emacs resaltar� el texto situado entre el cursor y la posici�n en la que tecle� C-<SPC>.  Finalmente, teclee C-w.  Eso elimina todo el texto entre las dos posiciones.

La diferencia entre �eliminar� y �borrar� es que el texto �eliminado� puede ser reinsertado (en cualquier posici�n), mientras que las cosas �borradas� no pueden ser reinsertadas (sin embargo, es posible deshacer el borrado; ver m�s abajo).  La reinserci�n de texto eliminado se llama �yanking� o �pegar�.

*** Copiar y pegar <yanking>

Reinsertar texto eliminado se denomina �yanking� o �pegar�.  (Piense en ello como pegar de nuevo, o traer de vuelta, alg�n texto que le fue quitado.)  Puede pegar el texto eliminado, ya sea el lugar en que fue eliminado, o en otra parte del buffer, o hasta en un archivo diferente.  Puede pegar el texto varias veces, lo que hace varias copias de �l.  Algunos editores se refieren a eliminar y reinsertar como �cortar� y �pegar� (consulte el Glosario en el manual de Emacs).

El comando para pegar es C-y.  Reinserta el �ltimo texto eliminado, en la posici�n actual del cursor.

�Qu� hacer si tiene alg�n texto que quiere pegar, y entonces elimina otra cosa?  C-y pegar�a la eliminaci�n m�s reciente.  Pero el texto previo no est� perdido.  Puede regresar a �ste usando el comando M-y. Despu�s de haber tecleado C-y para conseguir la eliminaci�n m�s reciente, tecleando M-y reemplaza el texto pegado con la eliminaci�n previa.  Tecleando M-y una y otra vez traer� las eliminaciones anteriores.  Cuando haya encontrado el texto que buscaba, no tiene que hacer nada para conservarlo.  Solo siga con su edici�n, dejando el texto pegado en donde est�.

Si teclea M-y suficientes veces, regresa al punto inicial (la eliminaci�n m�s reciente).

En resumen los comandos son:

	C-y  		Pega el texto <eliminado> o copiado
	M-y  		Una vez presionado C-y, reemplaca el texto pegado con la eliminaci�n previa
	M-w  		Copia el texto delimitado por una �rea marcada (usando C-<SPC>)

*** Deshacer y rehacer

Si hace un cambio al texto, y luego decide que fue un error, puede deshacer el cambio con el comando deshacer, C-_

	C-_  		Deshace cambios hechos al texto, tambi�n puede usar el comando C-/
	C-M-_  		Rehace el cambio hecho (si inicialmente se deshizo un cambio)


** Personalizaci�n de Emacs

Hay muchos paquetes que te ayudan a personalizar la experiencia en Emacs.

Aqu� dejo un archivo de configuraci�n b�sico: [[file:init.el][Configuraci�n b�sica para Emacs]]


** �Qu� m�s puedo hacer?

- Modos (modes) y modos menores (minor modes)
- Calendario
- Correo
- Lector de noticias
- IRC / mensajer�a
- Navegaci�n web
- Debugger
- Personalizaci�n de IDE
- Magit (interface para git)
- Org mode

[[file:~/Proyectos/intro_2_emacs/real_programmers.png][Emacs comic]]
