
(setq inhibit-startup-message t)    ;Quita el mensaje/pantalla de inicio

(scroll-bar-mode -1)     ;Esconde el scrollbar
(tool-bar-mode -1)       ;Escondo la toolbar
(tooltip-mode -1)        ;Esconde los tooltips
(set-fringe-mode 15)     ;Da un poco de espacio en los márgenes

(menu-bar-mode -1)       ;Esconde la barra de menú

;;Configura la campana
(setq visible-bell t)

;;Configura la font
(set-face-attribute 'default nil :font "Liberation Mono" :height 120)

;;Configura un tema
(load-theme 'tango-dark)

;;Le da a la tecla ESC la funcionalidad del comando C-g
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;;Configura utf-8
(define-coding-system-alias 'UTF-8 'utf-8)

;;Inicializa las fuentes de paquetes
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;;Inicializa use-package en sistema que no son Linux
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;;Paquete para ver los comandos en la pantalla, "C-c o" para verlos
(use-package command-log-mode
  :config
  (global-command-log-mode 1))


;;Despliega los números de los renglones
(column-number-mode)
(global-display-line-numbers-mode t)
;;Lo desabilita para ciertos modos
(dolist (mode '(org-mode-hook
		term-mode-hook
		eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;;Ivy package para mejor experiencia en Emacs
(use-package swiper :ensure t)
(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
	 :map ivy-minibuffer-map
	 ("TAB" . ivy-alt-done))
  :config
  (ivy-mode 1))

;;Doom modeline
;;Después de instalar el paquete all-the-icons recuerda correr el comando M-x all-the-icons-install-fonts
;;Si usas Linux o MacOS, listo
;;Si usas Windows, sigue estas instrucciones https://github.com/doomemacs/doomemacs/issues/2575
(use-package all-the-icons
  :ensure t)

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-gruvbox t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

;;Install doom-modeline to modify modeline
;;Icons are not working correctly, so turn them off temporarily t / nil
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 30)
	   (doom-modeline-icon t)
	   (doom-modeline-major-mode-icon t)
	   (doom-modeline-major-mode-color-icon t)
	   (doom-modeline-buffer-state-icon t)))

;;Instala rainbow-delimiters para hacer distinción de colores en parentesis
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;;Instala which-key para ver posibles teclas para comandos/atajos de teclado
(use-package which-key
  :init (which-key-mode)
  :config
  (setq which-key-idle-delay 1))

;;Instala counsel para obtener descripciones al usar M-x
(use-package counsel :ensure t
  :init
  :bind (("M-x" . counsel-M-x)
	 ("C-x b" . counsel-ibuffer)
	 ("C-x C-f" . counsel-find-file)
	 ("C-M-j" . 'counsel-switch-buffer)
	 :map minibuffer-local-map
	 ("C-r" . 'counsel-minibuffer-history))
  :config
  (counsel-mode 1)
  (setq ivy-initial-inputs-alist nil))

;;Instala ivy-rich para obtener más detalles de los comandos
(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

;;Instala helpful para obtener mejor documentación
(use-package helpful :ensure t)

;;Instala highlight-indent-guides para obtener guías con la indentación
(use-package highlight-indent-guides
  :ensure t
  :config
  ;; (highlight-indent-guides-mode 1)
  (setq highlight-indent-guides-method 'character))
(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)

